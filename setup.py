import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
    'Babel',
    'lingua',
    'pyramid',
    'pyramid_chameleon',
    'pyramid_fanstatic',
    'pyramid_debugtoolbar',
    'pyramid_tm',
    'colander',
    'deform',
    'js.deform',
    'js.bootstrap',
    'SQLAlchemy',
    'transaction',
    'zope.sqlalchemy',
    'waitress',
    ]

setup(name='Mfg',
      version='0.0',
      description='Mfg',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
          "Programming Language :: Python",
          "Framework :: Pyramid",
          "Topic :: Internet :: WWW/HTTP",
          "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
          ],
      author='',
      author_email='',
      url='',
      keywords='web wsgi bfg pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='mfg',
      install_requires=requires,
      message_extractors={'./mfg': [
          ('**.py', 'lingua_python', None),
          ('**.pt', 'lingua_xml', None),
      ]},
      entry_points="""\
      [paste.app_factory]
      main = mfg.run:main
      # Fanstatic resource library
      [fanstatic.libraries]
      mfg = mfg.resources:library
      [console_scripts]
      initialize_Mfg_db = mfg.scripts.initializedb:main
      pserve-fanstatic = mfg.resources:pserve
      """,
      )
