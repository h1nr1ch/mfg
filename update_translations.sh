#! /bin/bash

# extract messages from code and templates to pot file
pot-create -o mfg/locale/mfg.pot mfg

cd mfg/locale

# initialize po file once
#msginit -l de -o de/LC_MESSAGES/mfg.po

# merge new messages from pot to po file
msgmerge --update de/LC_MESSAGES/mfg.po mfg.pot

# compile mo files
msgfmt de/LC_MESSAGES/mfg.po -o de/LC_MESSAGES/mfg.mo

