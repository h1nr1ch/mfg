from sqlalchemy import Table, Column, String, Integer, Date
from mfg.db import metadata

__table_args__ = {'extend_existing': True}

offers = Table('offers', metadata,
    Column('id', Integer, primary_key=True),
    Column('start', String(200), nullable=False),
    Column('destination', String(200), nullable=False),
    Column('date', Date, nullable=False),
    Column('name', String(50), nullable=False),
    Column('phone', String(50), nullable=False),
    Column('start_time', String(20), nullable=False),
    Column('description', String(1000), nullable=True),
    Column('password', String(20), nullable=False),
)

interests = Table('interests', metadata,
    Column('id', Integer, primary_key=True),
    Column('start', String(200), nullable=False),
    Column('destination', String(200), nullable=False),
    Column('date', Date, nullable=False),
    Column('name', String(50), nullable=False),
    Column('phone', String(50), nullable=False),
    Column('start_time', String(20), nullable=False),
    Column('description', String(1000), nullable=True),
    Column('password', String(20), nullable=False),
)
