# -*- coding: utf-8 -*-
from mfg.orm_manager import ORMManager
from js.bootstrap import bootstrap
from mfg.views.base import BaseView
import deform
import js.deform
from mfg.views.formstuff import CheckPasswordSchema, InterestSchema
from mfg.run import _
import webob
import copy

ADMIN_PASS = 'UweSeeler09'
WRONG_PASS_MSG = u"Das eingegebene Passwort war leider nicht richtig!"
CHANGE_SUCCESS_MSG = u"Die Änderung wurde gespeichert."
DELETE_SUCCESS_MSG = u"Der Eintrag wurde gelöscht."


class ShowInterestView(BaseView):
    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.orm_manager = ORMManager()
        bootstrap.need()

    def __call__(self):
        id = self.request.matchdict['id']
        interest = self.orm_manager.retrieve_interest_by_id(id)
        edit_appstruct = copy.deepcopy(interest.__dict__)
        edit_appstruct.pop('password')
        ret = dict(
            entry=interest,
            entry_type=_('Interest'),
            edit_form=self._edit_form().render(appstruct=edit_appstruct),
            delete_form=self._del_form()
                            .render({'id': id, 'do': 'delete'}),
        )

        controls = self.request.POST
        if 'submit_changes' in controls or 'delete_definitely' in controls:
            if not self._check_password(interest, controls.get('password')):
                self.request.session.flash('danger:%s' % WRONG_PASS_MSG)
                return ret

        if 'submit_changes' in controls:
            updated_interest = self.orm_manager.update_entry(interest, controls)
            ret['entry'] = updated_interest
            self.request.session.flash('info:%s' % CHANGE_SUCCESS_MSG)

        if 'delete_definitely' in controls:
            self.orm_manager.delete_interest_by_id(interest.id)
            self.request.session.flash('info:%s' % DELETE_SUCCESS_MSG)
            url = self.request.route_url('list')
            return webob.exc.HTTPFound(location=url)

        return ret

    def _check_password(self, interest, passwd):
        return interest and (passwd == ADMIN_PASS or passwd == interest.password)

    def _del_form(self):
        schema = CheckPasswordSchema()
        form = deform.Form(
            schema.bind(request=self.request),
            buttons=(_('delete definitely'),))
        js.deform.auto_need(form)
        return form

    def _edit_form(self):
        schema = InterestSchema()
        form = deform.Form(
            schema.bind(request=self.request),
            buttons=(_('submit changes'),))
        js.deform.auto_need(form)
        return form


def includeme(config):
    showinterest_name = "showinterest"
    showinterest_route_path = "/%s/{id}" % showinterest_name
    config.add_route(
        showinterest_name,
        showinterest_route_path,
    )
    config.add_view(
        ShowInterestView,
        route_name=showinterest_name,
        renderer='mfg:templates/showentry.pt',
    )
