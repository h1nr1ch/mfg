from mfg.orm_manager import ORMManager
from js.bootstrap import bootstrap
from mfg.views.base import BaseView
import deform
import js.deform
from pyramid.httpexceptions import HTTPFound
from mfg.views.formstuff import InterestSchema, minimal_input_timestamp
from mfg.run import _


class AddInterestView(BaseView):
    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.orm_manager = ORMManager()
        bootstrap.need()

    def __call__(self):
        form = self.entry_form(InterestSchema())
        rendered_form = form.render({'timestamp': minimal_input_timestamp()})
        ret = dict(
            title=_('Add Interest'),
            form=rendered_form
        )

        if 'submit' in self.request.params:
            try:
                appstruct = form.validate(self.request.POST.items())
            except deform.ValidationFailure as e:
                ret['form'] = e.render()
                return ret

            new_interest = self.orm_manager.add_interest(
                start=appstruct['start'],
                destination=appstruct['destination'],
                date=appstruct['date'],
                name=appstruct['name'],
                phone=appstruct['phone'],
                start_time=appstruct['start_time'],
                description=appstruct['description'],
                password=appstruct['password']
            )

            url = self.request.route_url('showinterest', id=new_interest.id)
            return HTTPFound(url)

        return ret

    def entry_form(self, schema):
        form = deform.Form(
            schema.bind(request=self.request),
            buttons=('submit',))
        js.deform.auto_need(form)
        return form


def includeme(config):
    addinterest_name = "addinterest"
    addinterest_route_path = "/%s" % addinterest_name
    config.add_route(
        addinterest_name,
        addinterest_route_path,
    )
    config.add_view(
        AddInterestView,
        route_name=addinterest_name,
        renderer='mfg:templates/form.pt',
    )
