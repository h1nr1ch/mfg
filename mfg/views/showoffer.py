# -*- coding: utf-8 -*-
from mfg.orm_manager import ORMManager
from js.bootstrap import bootstrap
from mfg.views.base import BaseView
import deform
import js.deform
from mfg.views.formstuff import CheckPasswordSchema, OfferSchema
from mfg.run import _
import webob
import copy

ADMIN_PASS = 'UweSeeler09'
WRONG_PASS_MSG = u"Das eingegebene Passwort war leider nicht richtig!"
CHANGE_SUCCESS_MSG = u"Die Änderung wurde gespeichert."
DELETE_SUCCESS_MSG = u"Der Eintrag wurde gelöscht."


class ShowOfferView(BaseView):
    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.orm_manager = ORMManager()
        bootstrap.need()

    def __call__(self):
        id = self.request.matchdict['id']
        offer = self.orm_manager.retrieve_offer_by_id(id)
        edit_appstruct = copy.deepcopy(offer.__dict__)
        edit_appstruct.pop('password')
        ret = dict(
            entry=offer,
            entry_type=_('Offer'),
            edit_form=self._edit_form().render(appstruct=edit_appstruct),
            delete_form=self._del_form()
                            .render({'id': id, 'do': 'delete'}),
        )
        controls = self.request.POST

        if 'submit_changes' in controls or 'delete_definitely' in controls:
            if not self._check_password(offer, controls.get('password')):
                self.request.session.flash('danger:%s' % WRONG_PASS_MSG)
                return ret

        if 'submit_changes' in controls:
            updated_offer = self.orm_manager.update_entry(offer, controls)
            ret['entry'] = updated_offer
            self.request.session.flash('info:%s' % CHANGE_SUCCESS_MSG)

        if 'delete_definitely' in controls:
            self.orm_manager.delete_offer_by_id(offer.id)
            self.request.session.flash('info:%s' % DELETE_SUCCESS_MSG)
            url = self.request.route_url('list')
            return webob.exc.HTTPFound(location=url)

        return ret

    def _check_password(self, offer, passwd):
        return offer and (passwd == ADMIN_PASS or passwd == offer.password)

    def _del_form(self):
        schema = CheckPasswordSchema()
        form = deform.Form(
            schema.bind(request=self.request),
            buttons=(_('delete definitely'),))
        js.deform.auto_need(form)
        return form

    def _edit_form(self):
        schema = OfferSchema()
        form = deform.Form(
            schema.bind(request=self.request),
            buttons=(_('submit changes'),))
        js.deform.auto_need(form)
        return form


def includeme(config):
    showoffer_name = "showoffer"
    showoffer_route_path = "/%s/{id}" % showoffer_name
    config.add_route(
        showoffer_name,
        showoffer_route_path,
    )
    config.add_view(
        ShowOfferView,
        route_name=showoffer_name,
        renderer='mfg:templates/showentry.pt',
    )
