from mfg.orm_manager import ORMManager
from js.bootstrap import bootstrap
from mfg.views.base import BaseView
from mfg.run import _


class ListView(BaseView):
    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.orm_manager = ORMManager()
        bootstrap.need()

    def __call__(self):
        offers = self.orm_manager.retrieve_all_offers()
        interests = self.orm_manager.retrieve_all_interests()
        return dict(
            title=_('List View'),
            offers=offers,
            interests=interests)


def includeme(config):
    list_name = "list"
    list_route_path = "/"
    config.add_route(
        list_name,
        list_route_path,
    )
    config.add_view(
        ListView,
        route_name=list_name,
        renderer='mfg:templates/list.pt',
    )
