from mfg.layout import Layout

class BaseView(object):

    @property
    def global_layout(self):
        layout = Layout()
        return layout.global_layout

