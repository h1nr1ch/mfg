from mfg.orm_manager import ORMManager
from js.bootstrap import bootstrap
from mfg.views.base import BaseView
import deform
import js.deform
from pyramid.httpexceptions import HTTPFound
from mfg.views.formstuff import OfferSchema, minimal_input_timestamp
from mfg.run import _


class AddOfferView(BaseView):
    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.orm_manager = ORMManager()
        bootstrap.need()

    def __call__(self):
        form = self.entry_form(OfferSchema())
        rendered_form = form.render({'timestamp': minimal_input_timestamp()})
        ret = dict(
            title=_('Add Offer'),
            form=rendered_form
        )

        if 'submit' in self.request.params:
            try:
                appstruct = form.validate(self.request.POST.items())
            except deform.ValidationFailure as e:
                ret['form'] = e.render()
                return ret

            new_offer = self.orm_manager.add_offer(
                start=appstruct['start'],
                destination=appstruct['destination'],
                date=appstruct['date'],
                name=appstruct['name'],
                phone=appstruct['phone'],
                start_time=appstruct['start_time'],
                description=appstruct['description'],
                password=appstruct['password']
            )

            url = self.request.route_url('showoffer', id=new_offer.id)
            return HTTPFound(url)

        return ret

    def entry_form(self, schema):
        form = deform.Form(
            schema.bind(request=self.request),
            buttons=('submit',))
        js.deform.auto_need(form)
        return form


def includeme(config):
    addoffer_name = "addoffer"
    addoffer_route_path = "/%s" % addoffer_name
    config.add_route(
        addoffer_name,
        addoffer_route_path,
    )
    config.add_view(
        AddOfferView,
        route_name=addoffer_name,
        renderer='mfg:templates/form.pt',
    )
