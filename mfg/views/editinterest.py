from mfg.orm_manager import ORMManager
from js.bootstrap import bootstrap
from mfg.views.base import BaseView
import deform
import js.deform
from pyramid.httpexceptions import HTTPFound
from mfg.views.formstuff import InterestSchema, minimal_input_timestamp
from mfg.run import _


class EditInterestView(BaseView):
    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.orm_manager = ORMManager()
        bootstrap.need()

    def __call__(self):
        id = self.request.matchdict['id']
        interest = self.orm_manager.retrieve_interest_by_id(id)
        form = self.entry_form(InterestSchema())
        #form.action = self.request.url
        rendered_form = form.render(dict(
            start=interest.start,
            destination=interest.destination,
            date=interest.date,
            start_time=interest.start_time,
            name=interest.name,
            phone=interest.phone,
            description=interest.description,
            timestamp=minimal_input_timestamp()))

        ret = dict(
            title=_('Edit Interest'),
            form=rendered_form
        )

        if 'submit' in self.request.params:
            try:
                appstruct = form.validate(self.request.POST.items())
            except deform.ValidationFailure as e:
                ret['form'] = e.render()
                return ret

            self.orm_manager.update_interest(
                id=id,
                start=appstruct['start'],
                destination=appstruct['destination'],
                date=appstruct['date'],
                name=appstruct['name'],
                phone=appstruct['phone'],
                start_time=appstruct['start_time'],
                description=appstruct['description']
            )

            url = self.request.route_url('showinterest', id=id)
            return HTTPFound(url)

        return ret

    def entry_form(self, schema):
        form = deform.Form(
            schema.bind(request=self.request),
            buttons=('submit',))
        js.deform.auto_need(form)
        return form


def includeme(config):
    editinterest_name = "editinterest"
    editinterest_route_path = "/%s/{id}" % editinterest_name
    config.add_route(
        editinterest_name,
        editinterest_route_path,
    )
    config.add_view(
        EditInterestView,
        route_name=editinterest_name,
        renderer='mfg:templates/form.pt',
    )
