import colander
import deform
from datetime import datetime, timedelta
from base64 import b64decode, b64encode
from mfg.run import _


MINIMAL_FORM_SUBMISSION_DELAY = 1


def empty_validator(node, value):
    if value:
        raise colander.Invalid('form field should not be filled')


def check_timestamp(node, value):
    try:
        ts = b64decode(value)
        min_input_timestamp = datetime.strptime(ts, "%H:%M:%S.%f").time()
    except:
        raise colander.Invalid('is not a base64 encoded timestamp')
    if datetime.utcnow().time() < min_input_timestamp:
        raise colander.Invalid('form was posted to fast')


def minimal_input_timestamp():
    return b64encode(
        (datetime.utcnow()
        + timedelta(seconds=MINIMAL_FORM_SUBMISSION_DELAY))
        .time().isoformat())


class MfgSchema(colander.MappingSchema):
    start = colander.SchemaNode(
        colander.String(),
        title=_('start:')
    )
    destination = colander.SchemaNode(
        colander.String(),
        title=_('destination:')
    )
    date = colander.SchemaNode(
        colander.Date(),
        widget=deform.widget.DatePartsWidget(
            size='30',
        ),
        title=_('date:')
    )
    name = colander.SchemaNode(
        colander.String(),
        title=_('driver:')
    )
    phone = colander.SchemaNode(
        colander.String(),
        title=_('phone or email:')
    )
    start_time = colander.SchemaNode(
        colander.String(),
        title=_('departure time:')
    )
    description = colander.SchemaNode(
        colander.String(),
        missing=unicode(''),
        title=_('further information (meeting point, number of free seats, expected arrival time etc.):'),
        validator=colander.Length(max=5000),
        widget=deform.widget.TextAreaWidget(rows=3, cols=100),
        )
    password = colander.SchemaNode(
        colander.String(),
        title=_('password'),
        widget=deform.widget.PasswordWidget(size=20),
    )
    code = colander.SchemaNode(
            colander.String(),
            widget=deform.widget.HiddenWidget(),
            validator=empty_validator,
            missing=None)
    timestamp = colander.SchemaNode(
        colander.String(),
        widget=deform.widget.HiddenWidget(),
        validator=check_timestamp,
        missing=None
    )

class OfferSchema(MfgSchema):
    pass

class InterestSchema(MfgSchema):
    pass


class CheckPasswordSchema(colander.MappingSchema):
    password = colander.SchemaNode(
        colander.String(),
        title=_('password'),
        widget=deform.widget.PasswordWidget(size=20),
    )
    do = colander.SchemaNode(
        colander.String(),
        widget=deform.widget.HiddenWidget(),
    )
    id = colander.SchemaNode(
        colander.Integer(),
        widget=deform.widget.HiddenWidget(),
    )
