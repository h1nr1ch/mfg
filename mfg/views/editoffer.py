from mfg.orm_manager import ORMManager
from js.bootstrap import bootstrap
from mfg.views.base import BaseView
import deform
import js.deform
from pyramid.httpexceptions import HTTPFound
from mfg.views.formstuff import OfferSchema, minimal_input_timestamp
from mfg.run import _


class EditOfferView(BaseView):
    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.orm_manager = ORMManager()
        bootstrap.need()

    def __call__(self):
        id = self.request.matchdict['id']
        offer = self.orm_manager.retrieve_offer_by_id(id)
        form = self.entry_form(OfferSchema())
        #form.action = self.request.url
        rendered_form = form.render(dict(
            start=offer.start,
            destination=offer.destination,
            date=offer.date,
            start_time=offer.start_time,
            name=offer.name,
            phone=offer.phone,
            description=offer.description,
            timestamp=minimal_input_timestamp()))

        ret = dict(
            title=_('Edit Offer'),
            form=rendered_form
        )

        if 'submit' in self.request.params:
            try:
                appstruct = form.validate(self.request.POST.items())
            except deform.ValidationFailure as e:
                ret['form'] = e.render()
                return ret

            self.orm_manager.update_offer(
                id=id,
                start=appstruct['start'],
                destination=appstruct['destination'],
                date=appstruct['date'],
                name=appstruct['name'],
                phone=appstruct['phone'],
                start_time=appstruct['start_time'],
                description=appstruct['description'],
            )

            url = self.request.route_url('showoffer', id=id)
            return HTTPFound(url)

        return ret

    def entry_form(self, schema):
        form = deform.Form(
            schema.bind(request=self.request),
            buttons=('submit',))
        js.deform.auto_need(form)
        return form


def includeme(config):
    editoffer_name = "editoffer"
    editoffer_route_path = "/%s/{id}" % editoffer_name
    config.add_route(
        editoffer_name,
        editoffer_route_path,
    )
    config.add_view(
        EditOfferView,
        route_name=editoffer_name,
        renderer='mfg:templates/form.pt',
    )
