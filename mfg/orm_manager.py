from sqlalchemy import desc
from .db import DBSession
from .models import Offer, Interest


class ORMManager(object):
    """
    Encapsulates db access
    """

    def __init__(self):
        self.session = DBSession()

    def _retrieve_one(self, entry_type, id):
        if entry_type == 'offer':
            Class = Offer
        elif entry_type == 'interest':
            Class = Interest
        else:

            raise IOError('entry_type {0} is not defined'.format(entry_type))

        return self.session.query(Class).filter_by(id=id).one()


    def retrieve_all_offers(self):
        offers = self.session.query(Offer).order_by(Offer.date, Offer.start_time)
        return offers

    def add_offer(self, **kwargs):
        offer = Offer(**kwargs)
        self.session.add(offer)
        self.session.flush()
        return offer

    def update_entry(self, entry, props):
        props = dict(props)
        for k, v in props.items():
            hasattr(entry, k) and setattr(entry, k, v)

        self.session.flush()
        return entry

    def retrieve_offer_by_id(self, id):
        offer = self.session.query(Offer).filter_by(id=id).one()
        return offer

    def delete_offer_by_id(self, id):
        offer = self.session.query(Offer).filter_by(id=id).one()
        self.session.delete(offer)

    def retrieve_all_interests(self):
        interest = self.session.query(Interest).order_by(desc(Interest.date), Interest.start)
        return interest

    def add_interest(self, **kwargs):
        interest = Interest(**kwargs)
        self.session.add(interest)
        self.session.flush()
        return interest

    def retrieve_interest_by_id(self, id):
        interest = self.session.query(Interest).filter_by(id=id).one()
        return interest

    def delete_interest_by_id(self, id):
        interest = self.session.query(Interest).filter_by(id=id).one()
        self.session.delete(interest)
