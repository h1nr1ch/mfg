import os
from pyramid.config import Configurator
from pyramid.session import SignedCookieSessionFactory
from sqlalchemy import create_engine
from .db import DBSession, metadata
from pyramid.i18n import TranslationStringFactory
import deform
import pyramid.threadlocal

_ = TranslationStringFactory('mfg')


conf_defaults = {
    'mfg.base_includes': """
        mfg.views.list
        mfg.views.addoffer
        mfg.views.editoffer
        mfg.views.showoffer
        mfg.views.addinterest
        mfg.views.editinterest
        mfg.views.showinterest
    """,
}


# Set up the i18n machinerie
def translator(term):
    request = pyramid.threadlocal.get_current_request()
    # Attention: request could be None if called from a test
    if request is None:
        return term.interpolate() if hasattr(term, 'interpolate') else term
    else:
        return pyramid.i18n.get_localizer(request).translate(term)


def configure_deform():
    # deform templates dir
    search_path = (
        # my/custom/deform/templates,
        deform.template.default_dir,
    )

    deform.Form.default_renderer = deform.template.ZPTRendererFactory(
        search_path, translator=translator)


def main(global_config, **settings):
    for key, value in conf_defaults.items():
        if key in ('mfg.base_includes'):
            value = pyramid.settings.aslist(value)
        settings.setdefault(key, value)

    sqlalchemy_url = os.path.expandvars(settings.get('sqlalchemy.url'))
    engine = create_engine(sqlalchemy_url)
    DBSession.remove()
    DBSession.configure(bind=engine)
    metadata.bind = engine

    configure_deform()

    config = Configurator(
        settings=settings,
        )
    my_session_factory = SignedCookieSessionFactory('itsaseekreet')
    config.set_session_factory(my_session_factory)

    for module in settings['mfg.base_includes']:
        config.include(module)

    config.include('pyramid_chameleon')
    config.include('pyramid_fanstatic')
#    config.add_route('list', '/')
    config.add_route('add_contrib', '/add-contrib')
    config.add_route('add_interest', '/add-interest')
    config.add_route('single_contrib', '/contrib/{id}')
    config.add_route('voted_single_contrib', '/contrib/{id}/{voted}')
    config.add_route('single_interest', '/interest/{id}')
    config.add_route('voted_single_interest', '/interest/{id}/{voted}')
    config.add_route('edit_contrib', '/edit-contrib/{id}')
    config.add_route('edit_interest', '/edit-interest/{id}')
    config.add_route('vote', '/vote/{type}/{id}')
    config.add_static_view('deform_static', 'deform:static/')
    config.scan('mfg.views')
    config.add_translation_dirs(
        'mfg:locale',
        'colander:locale',
        'deform:locale',
        )
    return config.make_wsgi_app()
