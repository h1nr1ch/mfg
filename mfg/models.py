from pyramid.security import Allow, Everyone

from mfg.schema import offers, interests
from sqlalchemy.orm import mapper


class BaseModel(object):
    def __init__(self, **kwargs):
        allowed_keys = self._sa_class_manager.keys()
        for key in kwargs.keys():
            if hasattr(self, key) and key in allowed_keys:
                setattr(self, key, kwargs[key])


class Offer(BaseModel):
    pass


class Interest(BaseModel):
    pass


class Example(object):
    __acl__ = [(Allow, Everyone, 'view'),
               (Allow, 'group:editors', 'edit')]

    def __init__(self, request):
        pass

mapper(Offer, offers)
mapper(Interest, interests)
