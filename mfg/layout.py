from pyramid.renderers import get_renderer
from pyramid.decorator import reify


class Layout(object):
    @reify
    def global_layout(self):
        renderer = get_renderer("templates/global_layout.pt")
        return renderer.implementation().macros['layout']
